﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        //You can get a valid token at https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insert your JWT token INSERT_VALID_ACCESS_TOKEN
        static string INSERT_VALID_ACCESS_TOKEN = "TokenDeAutenticacao";
        static string kmsType = "BRYKMS";  //Valores Disponíveis: BRYKMS, GOVBR, DINAMO
        static string kmsPin = "SenhaDoCompartimentoDeCertificadosEmBase64Aqui";
        //static string kmsToken = "TokenPréAutorização/ATokenDinamo/TokenGovBR"
        static string kmsUuid = "UUIDDoCertificadoDesejadoAqui";
        //static string kmsUser = "12345678910"; 

        //Variáveis somente para caso seja certificado hospedado no HSM Dinamo
        //static string kmsPkey = "UUIDDaChavePrivada";
        //static string otp = "OTPDinamo";

        //Caso utilize alguma das credenciais comentadas, descomentar o envio delas (a partir da linha 116) e serialização (linha 250)

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AssinarKMS_Click(object sender, EventArgs e)
        {
            try
            {

                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                //We call the function to initialize
                byte[] responseInitialize = AssinarKMS().Result;

                string path = AppDomain.CurrentDomain.BaseDirectory;
                string fileName = "signature.pdf";
                File.WriteAllBytes(path + fileName, responseInitialize);

                if (responseInitialize == null)
                throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                //We populate the TextBoxSaidaInitializar with the data returned from the FW-HUB
                this.TextBoxSaidaInicializar.Text = "Assinatura gerada com sucesso no diretório " + path + fileName + ".";

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }


        public async static Task<byte[]> AssinarKMS()
        {
            var requestContent = new MultipartFormDataContent();

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./documento.pdf", FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            //configurando a imagem 

            var imageStream = new FileStream(path + "./imagem.jpg", FileMode.Open);
            var streamContentImage = new StreamContent(imageStream);

            DadosAssinatura dados_assinatura = new DadosAssinatura();

            dados_assinatura.algoritmoHash = "SHA256";
            dados_assinatura.perfil = "ADRB";

            string dados_assinaturaSerialized = Serialize(dados_assinatura);

            ConfiguracaoImagem configuracao_imagem = new ConfiguracaoImagem();

            configuracao_imagem.altura = 30;
            configuracao_imagem.largura = 100;
            configuracao_imagem.posicao = "INFERIOR_ESQUERDO";
            configuracao_imagem.pagina = "PRIMEIRA";
            configuracao_imagem.proporcaoImagem = 30;

            string configuracao_imagemSerialized = Serialize(configuracao_imagem);

            ConfiguracaoTexto configuracao_texto = new ConfiguracaoTexto();

            configuracao_texto.texto = "Assinado para Teste.";
            configuracao_texto.fonte = "COURIER";
            configuracao_texto.pagina = "PRIMEIRA";
            configuracao_texto.tamanhoFonte = 11;

            string configuracao_textoSerialized = Serialize(configuracao_texto);

            //Dados para acesso ao certificado no KMS
            KmsData kmsData = new KmsData();

            //Caso default é uso do PIN do compartimento do certificado no BRy KMS / Senha do Login de usuário DINAMO
            kmsData.pin = kmsPin;
            //Caso seja um token de pré autorização / Token GovBR ou AToken dinamo, descomentar abaixo e comentar a linha acima com o kmsPin
            //kmsData.token = kmsToken;
            //Caso seja um OTP dinamo, descomentar abaixo e comentar as linhas acima
            //kmsData.otp = otp;

            //UUID do certificado no BRy KMS / HSM Dinamo
            kmsData.uuid_cert = kmsUuid;

            //Caso a autenticação seja o CPF do BRy KMS ou o Login do HSM dinamo, descomentar abaixo
            //kmsData.user = kmsUser;

            //Caso seja HSM Dinamo, descomentar abaixo
            //kmsData.uuid_pkey = kmsPkey;

            string kms_data = Serialize(kmsData);

            requestContent.Add(new StringContent(kms_data), "kms_data");
            requestContent.Add(new StringContent(dados_assinaturaSerialized), "dados_assinatura");
            requestContent.Add(new StringContent(configuracao_imagemSerialized), "configuracao_imagem");
            requestContent.Add(new StringContent(configuracao_textoSerialized), "configuracao_texto");
            requestContent.Add(streamContentImage, "imagem", "imagem.jpg");
            requestContent.Add(streamContentDocument, "documento", "documento.pdf");

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);
            client.DefaultRequestHeaders.Add("kms_type", kmsType);
            

            //Envia os dados de inicialização da assinatura em formato Multipart
            var response = await client.PostAsync("https://hub2.bry.com.br/fw/v1/pdf/kms/assinaturas/", requestContent).ConfigureAwait(false);


            // O resultado do BRy Framework é transformado em um array de bytes para gravação do arquivo
            byte[] bytes = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);

            return bytes;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }

    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    public class DadosAssinatura
    {
        [DataMember]
        public string signatario;

        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public string perfil;
    }

    [DataContract]
    public class ConfiguracaoImagem
    {
        [DataMember]
        public int altura;

        [DataMember]
        public int largura;

        [DataMember]
        public int coordenadaX;

        [DataMember]
        public int coordenadaY;

        [DataMember]
        public string posicao;

        [DataMember]
        public string pagina;

        [DataMember]
        public int proporcaoImagem;
    }

    [DataContract]
    public class ConfiguracaoTexto
    {

        [DataMember]
        public int coordenadaX;

        [DataMember]
        public int coordenadaY;

        [DataMember]
        public string texto;

        [DataMember]
        public string pagina;

        [DataMember]
        public string fonte;

        [DataMember]
        public int tamanhoFonte;
    }

    [DataContract]
    public class KmsData
    {
        [DataMember]
        public string pin;

        [DataMember]
        public string uuid_cert;

        //Descomentar abaixo caso sejam utilizados as outras credenciais do KMS
      /*[DataMember] 
        public string uuid_pkey;

        [DataMember]
        public string user;

        [DataMember]
        public string token;

        [DataMember]
        public string otp; */
    }

}