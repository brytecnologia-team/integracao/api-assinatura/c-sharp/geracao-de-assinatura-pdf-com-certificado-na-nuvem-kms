# Geração de Assinadura Digital PDF com Certificado na Nuvem

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia C# ASP.NET 

Este exemplo apresenta os passos necessários para a geração de assinatura 
  - Passo 1: Montagem e envio da requisição ao HUB
  - Passo 2: Recebimento da resposta com arquivo assinado

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **PIN** ou **TOKEN**. Para credenciais com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem codificada em Base64, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

* *HSM Dinamo*: Certificado está armazenado em um HSM Dinamo. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **DINAMO** e o Valor da Credencial corresponde ao token de acesso, PIN da conta, ou OTP do usuário. É possível verificar um exemplo de como usar as credenciais do DINAMO em nosso [Repositório Swagger da API de Assinatura](https://hub2.bry.com.br/swagger-ui/index.html#/) no schema kms_data.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Este token de acesso pode ser obtido através da documentação disponibilizada em [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário em [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não seja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatória a posse de um certificado digital que identifica o autor do artefato assinado.

Por esse motivo, é necessário possuir algum certificado instalado em seu repositório local.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | 
| ------ | ------ | 
| INSERT_VALID_ACCESS_TOKEN | 
Token de acesso (access_token), obtido no BRyCloud (https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa  jurídica e para uso pessoal, da pessoa física. | 
| kmsType | Tipo de sistema de hospedagem de Certificados - Valores Disponíveis: BRYKMS, DINAMO | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs
| kmsPin | PIN do compartimento do certificados BRy KMS, ou senha do usuário no HSM Dinamo | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs
| kmsUuid | UUID do Certificado Desejado no BRy KMS ou no HSM Dinamo (não enviar junto com kmsUser) | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs
| kmsUser | CPF do usuário no BRy KMS ou Login de usuário do HSM Dinamo (não enviar junto com kmsUuid) | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs
| kmsToken | Token de Pré-Autorização BRy KMS / AToken do HSM Dinamo / Token de Autorização GOV.BR, caso seja usado, não usar kmsPin junto | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs
| kmsPkey | UUID da Chave Privada do certificado quando hospedado no HSM Dinamo | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs
| otp | One time password do HSM Dinamo. Não usar junto com kmsPin ou kmsToken | WebForm1.aspx.cs / WebForm2.aspx.cs / WebForm3.aspx.cs

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

    - Passo 1: Execute o Build da aplicação utilizando o Microsoft Visual Studio.
    - Passo 2: Certifique-se de que possui a BRy Extension instalada em seu navegador.
    - Passo 3: Execute o exemplo.

